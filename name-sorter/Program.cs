
using ClassLibrary;

namespace NameSorter
{
    class MainClass
    {
        /// <summary>
        /// Main process of the application
        /// </summary>
        /// <param name="args">
        /// An array of strings used in the program
        /// Index 0 : input file location / name
        /// </param>
        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Please make sure you include the input file destination.");
                return;
            }

            string inputFileName = args[0];
            string outputFileName = "sorted-names-list.txt";
            // Aditional Feature? Enable custom output location while providing default
            //args.Length > 1 ? args[1] : "sorted-names-list.txt";

            try
            {
                List<Person> people = ReadDataFromFile(args[0]);
                List<Person> sorted = SortData(people);
                WriteDataToFile(sorted, outputFileName);
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Read data in from the file at path "inputFileName".
        /// Data is read in as Person objects and added to a List
        /// This List is then returned
        /// </summary>
        /// <param name="inputFileName">Path to file containing names</param>
        /// <returns>A List of Person objects</returns>
        static List<Person> ReadDataFromFile(string inputFileName)
        {
            var people = new List<Person>();

            // Reading data in line by line via a StreamReader
            using var reader = new StreamReader(inputFileName);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                // Tries to create a new Person and add it to "people"
                try
                {
                    people.Add(new Person(line));
                }
                catch (InvalidNameException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return people;
        }

        /// <summary>
        /// Sorts the input List "people" using a <see cref="Sorter{T}"/> and returns the sorted List
        /// </summary>
        /// <param name="people">List of Person objects to be sorted</param>
        /// <returns>A sorted List of Person objects</returns>
        static List<Person> SortData(List<Person> people)
        {
            Sorter<Person> sorter = new MergeSorter<Person>(people);
            return sorter.GetSortedList();
        }

        /// <summary>
        /// Creates new / Overwrites file at desired path
        /// Prints all Persons in "people" to Screen and writes them to file
        /// </summary>
        /// <param name="people">List of Person objects to be ouptut</param>
        /// <param name="outputFileName">The filename to save the file as</param>
        static void WriteDataToFile(List<Person> people, string outputFileName)
        {
            string path = Directory.GetCurrentDirectory();
            string fileName = Path.Combine(path, outputFileName);

            // Create / Overwrite file
            FileStream output = File.Create(fileName);

            using var writer = new StreamWriter(output);

            // Writing each Person to Console and File
            foreach (Person person in people)
            {
                string toOutput = person.GetFullName();
                Console.WriteLine(toOutput);
                writer.WriteLine(toOutput);
            }
        }
    }
}
