using ClassLibrary;
using System.Collections.Generic;
using Xunit;

namespace name_sorter_unit_testing
{
    public class PersonClassTests
    {
        /// <summary>
        /// Checks if Exception thrown when too many given names are entered
        /// </summary>
        [Fact]
        public void TooManyGivenNames()
        {
            Assert.Throws<InvalidNameException>(
              () => new Person("Jesse James Terry Ryan Olson")
            );
        }

        /// <summary>
        /// Checks if Exception thrown when too many given names are entered
        /// </summary>
        [Fact]
        public void NoGivenNames()
        {
            Assert.Throws<InvalidNameException>(
              () => new Person("Olson")
            );
        }

        /// <summary>
        /// Check to see that Name is properly output with no whitespace
        /// </summary>
        [Fact]
        public void WorkingGetFullName()
        {
            var name = "Jesse James Terry Olson";
            var person = new Person("Jesse James Terry Olson");

            Assert.Equal(name, person.GetFullName());
        }
    }

    public class PersonIComparableTests
    {
        [Fact]
        public void LessThan_SameFirstName()
        {
            var person = new Person("Jesse Johnson");
            var other = new Person("Jesse Olson");

            Assert.Equal(-1, person.CompareTo(other));
        }

        [Fact]
        public void LessThan_DifferentSingleName()
        {
            var person = new Person("Jesse Olson");
            var other = new Person("Sophie Pablo");

            Assert.Equal(-1, person.CompareTo(other));
        }

        [Fact]
        public void LessThan_SameLastName()
        {
            var person = new Person("Jesse Olson");
            var other = new Person("Sophie Olson");

            Assert.Equal(-1, person.CompareTo(other));
        }

        [Fact]
        public void LessThan_SameName_AdditionalGivenName()
        {
            var person = new Person("Jesse Olson");
            var other = new Person("Jesse James Olson");

            Assert.Equal(-1, person.CompareTo(other));
        }

        [Fact]
        public void LessThan_DifferentMiddleName()
        {
            var person = new Person("Jesse Bob Olson");
            var other = new Person("Jesse James Olson");

            Assert.Equal(-1, person.CompareTo(other));
        }

        [Fact]
        public void LessThan_SameName_TwoAdditionalGivenNames()
        {
            var person = new Person("Jesse Olson");
            var other = new Person("Jesse James Terry Olson");

            Assert.Equal(-1, person.CompareTo(other));
        }

        [Fact]
        public void LessThan_SameTwoGivenNames_AdditionalMiddleName()
        {
            var person = new Person("Jesse James Olson");
            var other = new Person("Jesse James Terry Olson");

            Assert.Equal(-1, person.CompareTo(other));
        }

        //-- 0 if other is same
        [Fact]
        public void Equal_OneGivenName()
        {
            var person = new Person("Jesse Olson");
            var other = new Person("Jesse Olson");

            Assert.Equal(0, person.CompareTo(other));
        }

        [Fact]
        public void Equal_TwoGivenName()
        {
            var person = new Person("Jesse James Olson");
            var other = new Person("Jesse James Olson");

            Assert.Equal(0, person.CompareTo(other));
        }

        [Fact]
        public void Equal_ThreeGivenName()
        {
            var person = new Person("Jesse James Terry Olson");
            var other = new Person("Jesse James Terry Olson");

            Assert.Equal(0, person.CompareTo(other));
        }

        //-- 1 if other is less than
        [Fact]
        public void GreaterThan_SameFirstName()
        {
            var person = new Person("Jesse Olson");
            var other = new Person("Jesse Johnson");

            Assert.Equal(1, person.CompareTo(other));
        }

        [Fact]
        public void GreaterThan_DifferentSingleName()
        {
            var person = new Person("Jesse Olson");
            var other = new Person("Sophie Johnson");

            Assert.Equal(1, person.CompareTo(other));
        }

        [Fact]
        public void GreaterThan_SameLastName()
        {
            var person = new Person("Sophie Olson");
            var other = new Person("Jesse Olson");

            Assert.Equal(1, person.CompareTo(other));
        }

        [Fact]
        public void GreaterThan_DifferentMiddleName()
        {
            var person = new Person("Jesse James Olson");
            var other = new Person("Jesse Alex Olson");

            Assert.Equal(1, person.CompareTo(other));
        }

        [Fact]
        public void GreaterThan_SameName_TwoAdditionalGivenNames()
        {
            var person = new Person("Jesse Lance Terry Olson");
            var other = new Person("Jesse Olson");

            Assert.Equal(1, person.CompareTo(other));
        }

        [Fact]
        public void GreaterThan_SameName_AdditionalGivenNames()
        {
            var person = new Person("Jesse James Olson");
            var other = new Person("Jesse Olson");

            Assert.Equal(1, person.CompareTo(other));
        }

        [Fact]
        public void GreaterThan_SameTwoGivenNames()
        {
            var person = new Person("Jesse James Terry Olson");
            var other = new Person("Jesse Olson");

            Assert.Equal(1, person.CompareTo(other));
        }

        //-- 1 if other is null
        [Fact]
        public void OtherIsNull()
        {
            var person = new Person("Sophie Olson");

            Assert.Equal(1, person.CompareTo(other: null));
        }

    }

    public class BubbleSortTests
    {
        [Fact]
        public void PersonSuccessfullyAdded()
        {
            var sorter = new BubbleSorter<Person>();

            var p = new Person("Jesse Olson");

            sorter.AddItem(p);

            Assert.Contains(p, sorter.GetSortedList());
        }

        [Fact]
        public void PeopleSuccessfullyAdded()
        {
            var people = new List<Person>
            {
                new Person("Sophie Louise Olson"),
                new Person("Robert Pablo"),
                new Person("Sophie James Olson"),
                new Person("Jesse James Olson")
            };

            var sorter = new BubbleSorter<Person>();

            sorter.AddItems(people);

            Assert.Contains(people[0], sorter.GetSortedList());
            Assert.Contains(people[1], sorter.GetSortedList());
            Assert.Contains(people[2], sorter.GetSortedList());
            Assert.Contains(people[3], sorter.GetSortedList());
        }

        [Fact]
        public void PeopleSuccessfullySorted()
        {
            var p1 = new Person("Sophie Louise Olson");
            var p2 = new Person("Robert Pablo");
            var p3 = new Person("Sophie James Olson");

            var people = new List<Person>
            {
                p1,
                p2,
                p3
            };

            var sorter = new BubbleSorter<Person>(people);

            var sorted = new List<Person>
            {
                new Person("Sophie James Olson"),
                new Person("Sophie Louise Olson"),
                new Person("Robert Pablo")
            };

            Assert.Equal(sorted, sorter.GetSortedList());
        }

        [Fact]
        public void PersonSuccessfullySorted()
        {
            var people = new List<Person>
            {
                new Person("Sophie Louise Olson"),
                new Person("Robert Pablo"),
                new Person("Sophie James Olson"),
                new Person("Jesse James Olson")
            };

            var sorter = new BubbleSorter<Person>(people);

            var p = new Person("Jesse Olson");

            sorter.AddItem(p);

            var sorted = new List<Person>
            {
                p,
                new Person("Jesse James Olson"),
                new Person("Sophie James Olson"),
                new Person("Sophie Louise Olson"),
                new Person("Robert Pablo")
            };

            Assert.Equal(sorted, sorter.GetSortedList());
        }
    }

    public class MergeSortTest
    {
        [Fact]
        public void PersonSuccessfullyAdded()
        {
            var sorter = new MergeSorter<Person>();

            var p = new Person("Jesse Olson");

            sorter.AddItem(p);

            Assert.Contains(p, sorter.GetSortedList());
        }

        [Fact]
        public void PeopleSuccessfullyAdded()
        {
            var people = new List<Person>
            {
                new Person("Sophie Louise Olson"),
                new Person("Robert Pablo"),
                new Person("Sophie James Olson"),
                new Person("Jesse James Olson")
            };

            var sorter = new MergeSorter<Person>();

            sorter.AddItems(people);

            Assert.Contains(people[0], sorter.GetSortedList());
            Assert.Contains(people[1], sorter.GetSortedList());
            Assert.Contains(people[2], sorter.GetSortedList());
            Assert.Contains(people[3], sorter.GetSortedList());
        }

        [Fact]
        public void PeopleSuccessfullySorted()
        {
            var p1 = new Person("Sophie Louise Olson");
            var p2 = new Person("Robert Pablo");
            var p3 = new Person("Sophie James Olson");

            var people = new List<Person>
            {
                p1,
                p2,
                p3
            };

            var sorter = new MergeSorter<Person>(people);

            var sorted = new List<Person>
            {
                new Person("Sophie James Olson"),
                new Person("Sophie Louise Olson"),
                new Person("Robert Pablo")
            };

            Assert.Equal(sorted, sorter.GetSortedList());
        }

        [Fact]
        public void PersonSuccessfullySorted()
        {
            var people = new List<Person>
            {
                new Person("Sophie Louise Olson"),
                new Person("Robert Pablo"),
                new Person("Sophie James Olson"),
                new Person("Jesse James Olson")
            };

            var sorter = new MergeSorter<Person>(people);

            var p = new Person("Jesse Olson");

            sorter.AddItem(p);

            var sorted = new List<Person>
            {
                p,
                new Person("Jesse James Olson"),
                new Person("Sophie James Olson"),
                new Person("Sophie Louise Olson"),
                new Person("Robert Pablo")
            };

            Assert.Equal(sorted, sorter.GetSortedList());
        }
    }
}