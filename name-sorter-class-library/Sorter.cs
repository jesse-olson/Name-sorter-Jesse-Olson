namespace ClassLibrary
{
    /// <summary>
    /// The abstract Sorter class is a generic container used to store and sort a
    /// List of IComparable<T> Objects. 
    /// </summary>
    /// <remarks>
    /// The SortItems() method is the only method that children must override.
    /// </remarks>
    public abstract class Sorter<T> where T : IComparable<T>
    {
        protected List<T> sorted;

        /// <summary>
        /// Initializes a new instance of the <see cref="Sorter{T}"/> class.
        /// </summary>
        protected Sorter()
        {
            sorted = new List<T>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sorter{T}"/> class.
        /// Also performs a sort on the Objects held in "initialList".
        /// </summary>
        /// <param name="initialList">The initial List of Objects to be added to the sorter</param>
        public Sorter(List<T> initialList)
        {
            sorted = initialList;
            SortItems();
        }

        /// <summary>
        /// Adds the item "toAdd" to the "sorted" List and then proceeds to sort the
        /// List based on the sorting algorithm.
        /// </summary>
        /// <param name="toAdd">Object to be added to the "sorted" List.</param>
        public void AddItem(T toAdd)
        {
            sorted.Add(toAdd);
            SortItems();
        }

        /// <summary>
        /// Adds all the items contained in the "toAdd" List to the "sorted" List.
        /// After adding these values the "sorted" List is sorted based on the
        /// sorting algorithm.
        /// </summary>
        /// <param name="toAdd">A List of Objects to be added to the "sorted" List.</param>
        public void AddItems(List<T> toAdd)
        {
            sorted.AddRange(toAdd);
            SortItems();
        }

        /// <summary>
        /// To be implemented in the Child, this method should sort all of the values
        /// contained in the "sorted" List based on the Objects CompareTo method.
        /// </summary>
        public abstract void SortItems();

        /// <summary>
        /// Simple method to swap the position of two values within the "sorted" List.
        /// </summary>
        /// <param name="item1">The index of the first Object to be swapped.</param>
        /// <param name="item2">The index of the second Object to be swapped.</param>
        protected void Swap(int item1, int item2)
        {
            T temp = sorted[item1];
            sorted[item1] = sorted[item2];
            sorted[item2] = temp;
        }

        /// <summary>
        /// Returns the "sorted" List
        /// </summary>
        /// <returns>The "sorted" List</returns>
        public List<T> GetSortedList()
        {
            return sorted;
        }
    }
}