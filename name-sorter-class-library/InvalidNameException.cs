﻿namespace ClassLibrary
{
    /// <summary>
    /// Currently only used only within the Person Class. 
    /// Exception should only be thrown when the names used to instantiate a Person
    /// are not vaild
    /// </summary>
    public class InvalidNameException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidNameException"/> class.
        /// </summary>
        public InvalidNameException() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidNameException"/> class. 
        /// </summary>
        /// <param name="message">String used to convey reason for exception being thrown</param>
        public InvalidNameException(string message) : base(message) { }
    }
}
