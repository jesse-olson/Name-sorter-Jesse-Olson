namespace ClassLibrary
{
    /// <summary>
    /// The Person class currently holds all given and family names of a person.
    /// Inheriting from IComparable<Person>, you are able to compare the names of two people
    /// </summary>
    /// <remarks>
    /// Further work may include:
    /// - The addition of other attribute
    /// - Inclusion of Setter Methods for names
    /// - Public methods to get each name
    /// </remarks>
    public class Person : IComparable<Person>
    {
        //
        readonly List<string> names;

        // Index of a persons last name contained within the "names" List
        // Additionally used for the amount of given names a Person has
        public readonly int lastNameIndex;

        /// <summary>
        /// Initializes a new instance of the <see cref="Person{T}"/> class.
        /// </summary>
        /// <param name="fromTextFile">
        /// A string containing the both given and familial names seperated by spaces.
        /// i.e. "John Doe Smith"
        /// </param>
        public Person(string fromTextFile)
        {
            // Making sure that there is no leading or trailing whitespace
            var trimmedName = fromTextFile.Trim();

            var names = trimmedName.Split(' ');

            // Ensuring Person is valid, according to Assessment
            // i.e. 1 - 3 Given names and a Family name
            if (2 > names.Length)
            {
                throw new InvalidNameException("Missing Given or Family name");
            }
            else if (names.Length > 4)
            {
                throw new InvalidNameException("Person has too many given names");
            }

            lastNameIndex = names.Length - 1;
            this.names = new List<string>(names);
        }

        private int GetNumberOfGivenNames()
        {
            return lastNameIndex;
        }

        /// <summary>
        /// Returns all of the "given names" a Person has.
        /// </summary>
        /// <returns>A List containing all of the "given names" a person has.</returns>
        private List<string> GetGivenNames()
        {
            return names.GetRange(0, lastNameIndex);
        }

        /// <summary>
        /// Returns the "last name" of a person / final value in the "names" List
        /// </summary>
        /// <returns>Returns the final value in the "names" List</returns>
        private string GetLastName()
        {
            return names[lastNameIndex];
        }

        /// <summary>
        /// Returns a string formatted for print to both screen and an output file
        /// </summary>
        /// <returns>A formatted string housing the full name of the Person</returns>
        public string GetFullName()
        {
            string toReturn = "";
            foreach (string name in names) toReturn += name + " ";
            // Trimming "toReturn" as an extra ' ' was appended
            return toReturn.Trim();
        }

        /// <summary>
        /// A comparative method that overrides IComparable<T>. 
        /// Used to make a comparison between two Person instances.
        /// </summary>
        /// <param name="other">The Person instance to compare "this" Person to.</param>
        /// <returns>
        /// An int value based on how it compares to another Person.
        /// -1 : "other" is considered greater
        ///  0 : this and "other" are equal
        ///  1 : "other" is considered lesser
        /// </returns>
        public int CompareTo(Person other)
        {
            // A Person will always be greater than a null value
            if (other == null) return 1;

            // First comparison by last name as it holds the most weight
            int toReturn = GetLastName().CompareTo(other.GetLastName());
            if (toReturn != 0) return toReturn;

            // Getting the amount of given names both Person's are guarenteed
            // to contain
            int minNames = Math.Min(GetNumberOfGivenNames(), other.GetNumberOfGivenNames());

            var otherNames = other.GetGivenNames();
            for (int i = 0; i < minNames; i++)
            {
                // Making a comparison between given name
                toReturn = names[i].CompareTo(otherNames[i]);
                if (toReturn != 0) return toReturn;
            }

            // Finally if all names with a complimentary value are equal,
            // Return value is based on which Person has more given names
            if (GetNumberOfGivenNames() > other.GetNumberOfGivenNames()) return 1;
            if (GetNumberOfGivenNames() < other.GetNumberOfGivenNames()) return -1;
            return 0;
        }
    }
}