namespace ClassLibrary
{
    /// <summary>
    /// BubbleSorter Class is an extension of the Sorter Class.
    /// It utilises the Bubble sort to properly sort objects housed in the 'sorted' List
    /// </summary>
    /// <remarks>
    /// This Sorter is very slow and was the first Sorter developed.
    /// </remarks>
    public class BubbleSorter<T> : Sorter<T> where T : IComparable<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BubbleSorter{T}"/> class.
        /// </summary>
        public BubbleSorter() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BubbleSorter{T}"/> class.
        /// </summary>
        /// <param name="initialList">The initial List of Objects to be added to the sorter</param>
        public BubbleSorter(List<T> initialList) : base(initialList) { }

        // <summary>
        // Performs a Bubble sort on the items contained within the "sorted" List.
        // </summary>
        override public void SortItems()
        {
            var n = sorted.Count;

            while (n > 1)
            {
                var newn = 0;
                for (int i = 1; i < n; i++)
                {
                    // Swap the position of these Objects if the previous value is greater
                    if (sorted[i].CompareTo(sorted[i - 1]) < 0)
                    {
                        Swap(i, i - 1);
                        // Update "newn" to "i" for efficiency; last place a swap occured
                        newn = i;
                    }
                }
                n = newn;
            }
        }
    }
}