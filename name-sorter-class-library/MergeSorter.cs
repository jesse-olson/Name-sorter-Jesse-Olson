﻿namespace ClassLibrary
{
    /// <summary>
    /// MergeSorter Class is an extension of the Sorter Class.
    /// It utilises Mergesorting to properly sort objects housed in the 'sorted' List
    /// </summary>
    public class MergeSorter<T> : Sorter<T> where T : IComparable<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MergeSorter{T}"/> class.
        /// </summary>
        public MergeSorter() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="MergeSorter{T}"/> class.
        /// </summary>
        /// <param name="initialList">The initial List of Objects to be added to the sorter</param>
        public MergeSorter(List<T> initialList) : base(initialList) { }

        /// <summary>
        /// Sorts the sorted List using Merge sort algorithm
        /// </summary>
        public override void SortItems()
        {
            sorted = MergeSort(sorted);
        }

        /// <summary>
        /// Performs a Merge sort on an input List and returns the sorted list.
        /// This method is recursive
        /// </summary>
        /// <param name="toMerge">The List to be sorted</param>
        /// <returns>Returns a sorted List</returns>
        private List<T> MergeSort(List<T> toMerge)
        {
            // If there are no other objects to compare to return
            if (toMerge.Count == 1)
            {
                return toMerge;
            }

            var middle = toMerge.Count / 2;
            // Split the List in two and MergeSort each List
            List<T> firstHalf = MergeSort(toMerge.GetRange(0, middle));
            List<T> secondHalf = MergeSort(toMerge.GetRange(middle, toMerge.Count - middle));

            // Merge together both halves of the list
            return MergeSorter<T>.Merge(firstHalf, secondHalf);
        }

        /// <summary>
        /// This method merges together two Lists of objects sorting them using the ComparTo method. 
        /// </summary>
        /// <param name="firstList">The first List of Objects to be merged</param>
        /// <param name="secondList">The second List of Objects to be merged</param>
        /// <returns></returns>
        private static List<T> Merge(List<T> firstList, List<T> secondList)
        {
            // List to store the sorted objects in
            var result = new List<T>();

            // Initialising starting index for both Lists
            int firstIndex = 0;
            int secondIndex = 0;

            // Sifting through each List until one index is out of bounds
            while (firstList.Count > firstIndex && secondList.Count > secondIndex)
            {
                // Compare two objects and add the lesser value
                // In the case of equal Objects take from "firstList"
                int comp = firstList[firstIndex].CompareTo(secondList[secondIndex]);
                if (comp <= 0) result.Add(firstList[firstIndex++]);
                else result.Add(secondList[secondIndex++]);
            }

            // Add all remaining Objects to sorted list
            while (firstList.Count > firstIndex) result.Add(firstList[firstIndex++]);
            while (secondList.Count > secondIndex) result.Add(secondList[secondIndex++]);

            return result;
        }
    }
}
